﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Classe presa da Brackeys
/// </summary>
[RequireComponent(typeof(CharacterController))]
public class ThirdPersonMovement : MonoBehaviour
{

    public CharacterController controller;
    public Animator anim;
    public Transform cam;
    [Space(10)]
    public float speed = 6f;
    public float turnSmoothTIme=0.1f;
    public float gravity = 8.94f;
    [Space(10)]
    public bool isEnabled = true;

    private float horizontal;
    private float vertical;
    Vector3 dir;
    float targetAngle;
    float smoothAngle;
    Vector3 moveDir;

    float turnSmoothVelocity;

    // Update is called once per frame
    void Update()
    {
        if (!isEnabled)
            return;
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");

        dir = new Vector3(horizontal, 0, vertical);

        if (dir.magnitude >= 0.1f)
        {
            targetAngle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            smoothAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTIme);
            transform.rotation = Quaternion.Euler(0f, smoothAngle, 0f);

            moveDir = transform.forward;
        }
        else // altrimenti non si ferma
        {
            moveDir = Vector3.zero;
        }
        //Debug.Log("is controller grounded? " + controller.isGrounded);
        if(!controller.isGrounded)
        {
            moveDir.y -= gravity * Time.deltaTime;
        }
        anim.SetFloat("speed", dir.sqrMagnitude);
        controller.Move((moveDir)* speed*Time.deltaTime);
    }
}
