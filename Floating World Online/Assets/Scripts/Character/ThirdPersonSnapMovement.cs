﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

public class ThirdPersonSnapMovement : MonoBehaviour
{
    public CharacterController controller;
    public Animator anim;
    public Transform cam;

    public float speed = 6f;
    public float gravity = 8.94f;

    private float horizontal;
    private float vertical;
    float targetAngle;
    Vector3 dir;
    Vector3 moveDir;


    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");

        dir = new Vector3(horizontal, 0, vertical);

        if (dir.magnitude >= 0.1)
        {
            targetAngle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);
            dir = transform.forward;
        }

        anim.SetFloat("speed", dir.sqrMagnitude);
        //Debug.Log("is controller grounded? " + controller.isGrounded);
        if (!controller.isGrounded)
        {
            dir.y -= gravity * Time.deltaTime;
        }
        Debug.Log("Dir: "+dir);
        controller.Move(dir *speed* Time.deltaTime);
    }
}
