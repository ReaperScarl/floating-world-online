﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class responsible for the combat of the character.
/// The character and animation will be the Ellen from the 3d Character Pack from unity
/// </summary>
public class CharCombatManager : MonoBehaviour
{
    public Animator anim;
    public int maxCombo = 3;
    public float timeToReset=1;

    public int comboIndex =-1;
    float timeWaited;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (comboIndex >= maxCombo)
            {
                comboIndex = 0;
            }
            else
                ++comboIndex;
            Debug.Log($"combo index: {comboIndex}");
            anim.SetInteger("comboIndex", comboIndex);
            anim.SetTrigger("attack");
            
            timeWaited = 0;
        }

        timeWaited += Time.deltaTime;

        if (comboIndex >= 0 && timeWaited > timeToReset)
        {
            comboIndex = -1;
            anim.SetInteger("comboIndex", comboIndex);
        }
    }
}
