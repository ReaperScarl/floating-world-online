﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Kinda of effect that will be active for a limited time
/// </summary>
public class ActiveEffect : ScriptableObject
{
    public bool isPositiveEffect;
    public string description;
    public float timeActive;
    public Sprite mySprite;
    private float currentTime=0;
    public Effect[] effects; // the effects that will be used
    public bool isConcluded=false;


    /// <summary>
    /// Does something every second. 
    /// </summary>
    /// <param name="character"></param>
    public virtual void UpdateThisEffect(CharacterData character)
    {
        // if currTime=0, this is the first update.

        //if currTime >0 & <timeActive, the the update should do something only in per second effects

        // if this is the last second, it should give to the handler a signal to remove this effect
        // it should also rem
        if (currentTime == timeActive)
            isConcluded = true;
    }
}
