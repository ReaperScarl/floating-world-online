﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

public class CharController : MonoBehaviour
{
    public CharacterData myData;
    public CharUIManager charUIManager;
    public List<ActiveEffect> activeEffects= new List<ActiveEffect>();
    public ThirdPersonMovement charMovement;
	public LayerMask physicsInteractionMask;
	public float timeToPressToUse=1.5f;
	
    protected Camera myCamera;
    protected Interactible interactible;
    protected Interactible currFocus;
    protected WaitForSeconds statusWait = new WaitForSeconds(1);
    protected WaitForSeconds interactibleWait = new WaitForSeconds(0.5f);

    protected bool isInteractibleInProgress = false;
	protected float timeInteractionPressed=0;
    protected GeneralStatModHandlerManager statModManager = new GeneralStatModHandlerManager();
	

    public void Start()
    {
        StartCoroutine(UpdateActiveEffects());
        StartCoroutine(SurvivalEffects());
        myCamera = charMovement.cam.GetComponent<Camera>();
    }

    /// <summary>
    /// Method called by usable elements to be get
    /// </summary>
    /// <param name="effects"></param>
    public void SetEffects(Effect[] effects)
    {
        foreach(Effect effect in effects)
        {
            switch (effect.stat)
            {
                case (ActiveStatType.hp):
                    myData.currHP = Mathf.Clamp(myData.currHP + effect.value, 0, myData.CurrMaxHP);
                    break;
                case (ActiveStatType.hunger):
                    myData.currHunger = Mathf.Clamp(myData.currHunger + effect.value, 0, myData.maxHunger);
                    charUIManager.UpdateHunger(myData.currHunger, myData.maxHunger);
                    break;
                case (ActiveStatType.thirst):
                    myData.currThirst = Mathf.Clamp(myData.currThirst + effect.value, 0, myData.maxThirst);
                    charUIManager.UpdateThirst(myData.currThirst, myData.maxThirst);
                    break;
                case (ActiveStatType.sleep):
                    myData.currSleep = Mathf.Clamp(myData.currSleep + effect.value, 0, myData.maxSleep);
                    charUIManager.UpdateSleep(myData.currSleep, myData.maxSleep);
                    break;
            }
        }
    }

    /// <summary>
    /// Survival Effects per time. The hunger, sleep and thirst will go up during time
    /// </summary>
    /// <returns></returns>
    IEnumerator SurvivalEffects()
    {
        while (gameObject.activeInHierarchy)
        {
            myData.currHunger += myData.hungerPerMinute/60;
            myData.currThirst += myData.thirstPerMinute/60;
            myData.currSleep += myData.sleepPerMinute/60;

            charUIManager.UpdateSleep(myData.currSleep, myData.maxSleep);
            charUIManager.UpdateThirst(myData.currThirst, myData.maxThirst);
            charUIManager.UpdateHunger(myData.currHunger, myData.maxHunger);
            yield return statusWait;
        }
    }

    /// <summary>
    /// makes an update on all the effects going
    /// </summary>
    /// <returns></returns>
    IEnumerator UpdateActiveEffects()
    {
        while (gameObject.activeInHierarchy)
        {
            foreach(ActiveEffect effect in activeEffects)
            {
                effect.UpdateThisEffect(myData);

            }
            for (int i=0; i<activeEffects.Count;)
            {
                if (activeEffects[i].isConcluded) // if finished, delete it
                {
                    activeEffects.RemoveAt(i);
                }
                else
                    i++;
                

            }
            yield return statusWait;
        }
    }

    public void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        #region Mouse raycast
        // for now mouse position, after center of the camera
        Ray ray;
		//ray= myCamera.ScreenPointToRay(Input.mousePosition);
		ray= myCamera.ViewportPointToRay(new Vector3(0.5f,0.5f,0));
        RaycastHit hit;

        if (Physics.Raycast(ray,out hit, 50, physicsInteractionMask))
        {
            Debug.DrawLine(myCamera.transform.position, hit.point);
            interactible = hit.collider.GetComponent<Interactible>();
            if (interactible != null)
            {
                StartCoroutine(SetFocusAndWait());
            }
            else
            {
                if (currFocus != null)
                {
                    RemoveFocus();
                }
            }
        }
        #endregion
		
		#region Interaction 
		if(Input.GetKey(KeyCode.E))
		{
			timeInteractionPressed+=Time.deltaTime;
            if(currFocus &&currFocus.GetType()== typeof(PickUpUsableInteractible))
            {
                ((PickUpUsableInteractible)currFocus).UpdateUseSlider(timeInteractionPressed / timeToPressToUse);
                
                if (timeInteractionPressed > timeToPressToUse)
                {
                    UseCurrFocus();
                }
            }

        }
		if(Input.GetKeyUp(KeyCode.E))
		{
			if(currFocus)
			{
				if(timeInteractionPressed>timeToPressToUse)
				{
					UseCurrFocus();
				}
				else
				{
					if(timeInteractionPressed<=0.3f)
						InteractWithFocus();

				}
			}
			
			timeInteractionPressed=0;
			if(currFocus && currFocus.GetType() == typeof(PickUpUsableInteractible))
                ((PickUpUsableInteractible)currFocus).UpdateUseSlider(0);
        }
		#endregion

    }

    #region Interaction methods
    /// <summary>
    /// Called when a Interactible is on focus
    /// </summary>
    /// <param name="newFocus"></param>
    private void SetFocus(Interactible newFocus)
    {
        if(Vector3.Distance(transform.position, newFocus.transform.position) <= newFocus.radius)
        {
            if (currFocus != null && newFocus!=currFocus)
                currFocus.HideInteractionUI();
            if (currFocus != newFocus)
            {
                currFocus = newFocus;
                currFocus.ShowInteractionUI(this);
                //Debug.Log("Show focus message: " + currFocus);
            }
        }
    }

    /// <summary>
    /// Called when the current focus is no longer in focus
    /// </summary>
    private void RemoveFocus()
    {
        //Debug.Log("Remove focus from: " + currFocus);
        currFocus.HideInteractionUI();
        currFocus = null;
    }

    /// <summary>
    /// Instead of setting the focus immediately, wait between an interaction and the next call
    /// </summary>
    /// <returns></returns>
    IEnumerator SetFocusAndWait()
    {
        if(!isInteractibleInProgress)
        {
            isInteractibleInProgress = true;
            SetFocus(interactible);
            yield return interactibleWait;
            isInteractibleInProgress = false;
        }
    }
	
	// Method called to interact with a curr focus element (Class Interactible)
	protected void InteractWithFocus()
	{
		
		currFocus.Interact();
		Debug.Log("Interact with this Object");
	}
	
	// Method called to use the curr focus element
	protected void UseCurrFocus()
	{
		Debug.Log("Use this Object");
        if (currFocus.GetType() == typeof(PickUpUsableInteractible))
            ((PickUpUsableInteractible)currFocus).UseEquip();
    }
    #endregion

    public void SendFeedbackMessage(string message, FeedbackType typeMessage)
    {
        charUIManager.feedbakManager.SendMessage(message, typeMessage);
    }

    #region Equipments
    /// <summary>
    /// Method called by the CharEquipUIMenu to unequip and item.
    /// </summary>
    /// <param name="elementToUnequip"></param>
    public void UpdateEquipmentModifiers()
    {
        // here the modifiers from the equipment will be updated
        foreach(Equipment equip in myData.equipmentItems)
        {
            // update the currModifiers
            if(equip)
                statModManager.UpdateModifiers(equip, myData);
        }
    }

    #endregion


}
