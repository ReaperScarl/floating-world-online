﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class responsible for the activation and deactivation of the ragdoll.
/// </summary>
public class RagdollManager : MonoBehaviour
{

    public Collider [] normalColliders;

    // Start is called before the first frame update
    void Start()
    {
        DeactivateRagdoll();
    }

    /// <summary>
    /// Method called to activatee all the rigid body, colliders and configurable joints
    /// </summary>
    public void ActivateRagdoll()
    {
        foreach(Collider coll in normalColliders)
        {
            coll.enabled = false;
        }

        foreach (Collider coll in GetComponentsInChildren<Collider>())
        {
            coll.enabled = true;
        }
        foreach (Joint joint in GetComponentsInChildren<Joint>())
        {
            joint.enableCollision = true;
        }
        foreach (Rigidbody rb in GetComponentsInChildren<Rigidbody>())
        {
            rb.useGravity = true;
        }
    }

    /// <summary>
    /// Method called to deactivatee all the rigid body, colliders and configurable joints
    /// </summary>
    public void DeactivateRagdoll()
    {
        foreach(Collider coll in GetComponentsInChildren<Collider>())
        {
            coll.enabled = false;
        }
        foreach(Joint joint in GetComponentsInChildren<Joint>())
        {
            joint.enableCollision = false;
        }
        foreach (Rigidbody rb in GetComponentsInChildren<Rigidbody>())
        {
            rb.useGravity = false;
        }
    }
}
