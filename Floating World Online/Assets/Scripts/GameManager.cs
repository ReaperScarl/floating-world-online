﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class responsible for the game management.
/// Scraped the multiplayer Part, and only kept the part for the single player
/// </summary>
public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    //public static Dictionary<int, PlayerManager> players = new Dictionary<int, PlayerManager>();

    //public GameObject localPlayerPrefab;
    public GameObject player;

    public Camera playerCamera;
    public CinemachineFreeLook myPlayerCinecamera;

    public InGameMenuUI inGameMenu;

    protected bool isInGameActive=false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
            inGameMenu.gameObject.SetActive(false);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }




    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isInGameActive)
            {
                inGameMenu.CloseInGameMenu();

                isInGameActive = false;
               
            }
            else
            {
                inGameMenu.OpenInGameMenu();
                isInGameActive = true;
            }
           
        }

        # region Specific submenus

        // 0 : map

        // 1: quests

        // 2: Character-equipment

        //3: Inventory
        if (Input.GetKeyDown(KeyCode.I))
        {
            inGameMenu.OpenInGameMenu(3);
            isInGameActive = true;
        }

        //4: effects

        //5: options


        #endregion
        PauseResumeMovementCam(!isInGameActive);
    }

    /// <summary>
    /// Method called to stop the player movement and the camera Movement
    /// </summary>
    /// <param name="isResuming">when true, it is resuming move/ came, it will stop otherwise</param>
    protected void PauseResumeMovementCam(bool isResuming)
    {
        if (isResuming)
        {
            player.GetComponent<ThirdPersonMovement>().isEnabled = true;
            myPlayerCinecamera.enabled = true;
        }
        else
        {
            player.GetComponent<ThirdPersonMovement>().isEnabled = false;
            myPlayerCinecamera.enabled = false;


        }
    }

}
