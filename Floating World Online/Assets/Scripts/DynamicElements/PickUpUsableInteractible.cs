﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class used to manager the pickupable objects that are also usable/equippable
/// </summary>
public class PickUpUsableInteractible : PickUpInteractible
{

    public BarUIManager useSliderManager;

    public override void Interact()
    {
        base.Interact();
    }

    protected float currentUsePSliderProgress = 0;

    public override void UseEquip()
    {
        base.UseEquip();
        if (itemInfo.type == ObjectType.equipment)
        {
            // equip this item etc
            CharacterData data = charController.myData;
            if(data.equipmentItems[(int)((Equipment)itemInfo).equipType])
                data.equipmentItems[(int)((Equipment)itemInfo).equipType].RemoveEquipped();
            data.equipmentItems[(int)((Equipment)itemInfo).equipType] = (Equipment)itemInfo;
            charController.UpdateEquipmentModifiers();
        }
        else
        {
            // use this item etc.
            ((UsableObject)itemInfo).Use(charController);
        }
        GameObject.Destroy(this.gameObject);

    }

    /// <summary>
    /// Method called to update the pickupUsableObject with the curr value of time
    /// </summary>
    /// <param name="value"></param>
    public void UpdateUseSlider(float value)
    {
        useSliderManager.UpdateBar(value);
    }

}
