﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class that will be responsible for a generic interaction 
/// </summary>
public class Interactible : MonoBehaviour
{
    [Header("The radius of the interaction")]
    public float radius = 3f;
    public RectTransform interactionUI;
    public RectTransform infoUI;
    [Range(0.1f,10)]
    public float scalingSpeed = 1;
    protected Transform lookTarget;
    protected Coroutine currRoutine;

    protected bool isUIHidden=true;
    protected CharController charController;

    protected virtual void Start()
    {
        if(GameManager.instance && GameManager.instance.player)
        {
            lookTarget = GameManager.instance.playerCamera.transform;
            StartCoroutine(LookToPlayer());
        }
    }

    /// <summary>
    /// When called, it will show the UI of this interactible
    /// </summary>
    public virtual void ShowInteractionUI(CharController charContr)
    {
        //Debug.Log("Is in show interaction UI");
        charController = charContr;
        interactionUI.gameObject.SetActive(true);
        StartCoroutine(LookToPlayer());
        if (currRoutine != null)
            StopCoroutine(currRoutine);
        currRoutine=StartCoroutine(ShrinkUpOrDown(true));
    }

    /// <summary>
    /// When called, it will hide the UI of this interactible
    /// </summary>
    public virtual void HideInteractionUI()
    {
        //Debug.Log("Hide interaction UI");
        if (currRoutine != null)
            StopCoroutine(currRoutine);
        currRoutine=StartCoroutine(ShrinkUpOrDown(false));
        interactionUI.gameObject.SetActive(false);
        
    }

	// Method called to interact with this interactible
	public virtual void Interact()
	{
		
	}
	
	
    /// <summary>
    /// Method called to smoothly show or hide the interaction 
    /// </summary>
    /// <param name="isShrinking"> true: the UI is scaling up; false: the UI is scaling down</param>
    /// <returns></returns>
    IEnumerator ShrinkUpOrDown(bool isShrinking)
    {
        Debug.Log("Is in Shrink method: "+isShrinking);

        // if it is already shown or already hidden, don't do anything
        if (isShrinking == isUIHidden) 
        {
            isUIHidden = !isShrinking; // if it is scaling up, the ui is not hidden and viceversa
            if (isShrinking) // scaling up
            {
                interactionUI.localScale = Vector3.one * interactionUI.localScale.x;
                while (interactionUI.localScale.x < 1)
                {
                    interactionUI.localScale += Vector3.one * Time.deltaTime * scalingSpeed;
                    //Debug.Log("Local scale: " + interactionUI.localScale.x);
                    yield return null;
                }
            }
            else // Scaling down
            {
                interactionUI.localScale = Vector3.one*interactionUI.localScale.x;
                while (interactionUI.localScale.x > 0.05f)
                {
                    interactionUI.localScale -= Vector3.one * Time.deltaTime * scalingSpeed;
                    //Debug.Log("Local scale: " + interactionUI.localScale.x);
                    yield return null;
                }
            }
        }
    }

	
    /// <summary>
    /// It looks to the player every time, so that it is oriented to the player when needed
    /// </summary>
    protected IEnumerator LookToPlayer()
    {
        Vector3 dir;
        float targetAngle;
        while (gameObject.activeInHierarchy)
        {
            
            dir = lookTarget.position - transform.position;
            targetAngle= Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
            interactionUI.rotation = Quaternion.Euler(0f, targetAngle+180, 0f);
            infoUI.rotation = Quaternion.Euler(0f, targetAngle+180, 0f);
            
           yield return null;
        }   
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
