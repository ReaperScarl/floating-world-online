﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

/// <summary>
/// Class responsabile for all the elements that can be interacted to pick up stuff
/// </summary>
public class PickUpInteractible : Interactible
{
    public SimpleObject itemInfo;
    public GameObject myMesh;
    [Range(0,60)]
    public float rotationSpeed;
    public TextMeshProUGUI nameItem;
    public Image itemTypeIcon;
    public TypeIconSprites iconTypeSprites;
    public Color[] rarityColors = new Color []
    {
        Color.white,
        Color.green,
        Color.blue,
        Color.yellow,
        new Color(131/255,0,1), // purple
        new Color(1,145/255,0) // orange
    };

    protected override void Start()
    {
        base.Start();
        ShowItemInfo();
        StartCoroutine(RotateMesh());
    }


    public override void ShowInteractionUI(CharController charContr)
    {
        base.ShowInteractionUI(charContr);
    }

    /// <summary>
    /// Method called to pickUp an object and put it in an available space
    /// </summary>
    /// <param name="charController"></param>
    public void PickUp()
    {
        // put this item in the inventory if enough space available
        // delete this item in that case
        Bag thisBag = (Bag)GameManager.instance.player.GetComponent<CharacterData>().equipmentItems[(int)EquipType.backpack];
        CharacterData data = GameManager.instance.player.GetComponent<CharacterData>();
        if (itemInfo.type == ObjectType.equipment)
        {
            if (data.equipmentItems[(int)((Equipment)itemInfo).equipType]==null)
            {
                data.equipmentItems[(int)((Equipment)itemInfo).equipType] = (Equipment)itemInfo;
                charController.UpdateEquipmentModifiers();
                GameObject.Destroy(this.gameObject);
                return;
            }    
        }

        if (thisBag)
        {
            if(thisBag.Add(itemInfo))
            {
                //Debug.Log("Pickup");
                GameObject.Destroy(this.gameObject);
            }
            else
            {
                Debug.Log("Item couldn't be added");
            }
        }
        else
        {
            charController.SendFeedbackMessage("No bag found! Item could not be picked up!",FeedbackType.warning);
        }
    }

    public override void Interact()
    {
        base.Interact();
        PickUp();
    }

    public virtual void UseEquip()
    {

    }

    /// <summary>
    /// Method called to show info about the item (name, rarity)
    /// </summary>
    protected void ShowItemInfo()
    {
        nameItem.text = itemInfo.myName;
        nameItem.color = rarityColors[(int)itemInfo.myRarity];
        itemTypeIcon.sprite = iconTypeSprites.GetTypeIcon(itemInfo.type);
        itemTypeIcon.transform.position = new Vector3(nameItem.transform.position.x -(0.2f*nameItem.GetPreferredValues().x / 2), itemTypeIcon.transform.position.y, itemTypeIcon.transform.position.z);
        //Debug.Log($"character count of this item {nameItem.name} : {nameItem.GetPreferredValues(nameItem.text)} ");
    }


    protected IEnumerator RotateMesh()
    {
        while (gameObject.activeInHierarchy)
        {
            yield return null;
            myMesh.transform.rotation=Quaternion.Euler(myMesh.transform.rotation.eulerAngles+Vector3.up* rotationSpeed*Time.deltaTime);
        }
    }
}
