﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class responsible for the UI of the character
/// </summary>
public class CharUIManager : MonoBehaviour
{
    public BarUIManager sleepBar;
    public BarUIManager thirstBar;
    public BarUIManager hungerBar;


    public FeedbackListManager feedbakManager;

    // status icons

    // life

    // weapon active?

    //Update the sleep bar
    public void UpdateSleep(float currValue, float maxValue)
    {
        sleepBar.UpdateBar(currValue, maxValue);
    }

    //Update the sleep bar
    public void UpdateThirst(float currValue, float maxValue)
    {
        thirstBar.UpdateBar(currValue, maxValue);
    }

    //Update the sleep bar
    public void UpdateHunger(float currValue, float maxValue)
    {
        hungerBar.UpdateBar(currValue, maxValue);
    }

    

}
