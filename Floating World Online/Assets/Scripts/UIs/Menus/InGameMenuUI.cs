﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class responsible for the General Ingame Menu UI
/// </summary>
public class InGameMenuUI : MonoBehaviour
{
    [Header("The buttons of the general menu")]
    public Button[] generalInGameButtons;
    [Header("The menu gameobject to close or open when the button is selected. It should have the same number of the in game buttons")]
    public GameObject[] menus;

    public Color selectedColorButton;
    public Color unSelectedColorButton;

    public void ChangeMenuItem(int indexMenu)
    {
        for(int i=0; i <generalInGameButtons.Length; i++)
        {
            if (i != indexMenu)
            {
                generalInGameButtons[i].image.color = unSelectedColorButton;
                menus[i].SetActive(false);
            }
            else
            {
                generalInGameButtons[i].image.color = selectedColorButton;
                menus[i].SetActive(true);
                menus[i].GetComponent<SubMenuUIController>().UpdateThisMenu();
            }
        }
    }

    /// <summary>
    /// Method called to open this in game menu UI.
    /// If the index menu is not selected, it will open the first menu available (map)
    /// </summary>
    /// <param name="indexMenu"></param>
    public void OpenInGameMenu(int indexMenu=0)
    {
        gameObject.SetActive(true);
        ChangeMenuItem(indexMenu);
    }

    /// <summary>
    /// Close the in game menu
    /// </summary>
    public void CloseInGameMenu()
    {
        gameObject.SetActive(false);
    }
}
