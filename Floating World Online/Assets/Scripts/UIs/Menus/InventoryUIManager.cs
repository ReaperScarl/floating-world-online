﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

/// <summary>
/// Class responsible for the inventory sub folder.
/// </summary>
public class InventoryUIManager : SubMenuUIController
{
    
    public Button[] filterItemsButtons;
    [Header("The slot that will be equal for all the items")]
    public ItemSlotUI itemSlotPrefab;
    public Color selectedItem;
    public Color unSelectedItem;
    public TypeIconSprites typeIconSprites;
    [Space(10)]

    public TextMeshProUGUI bagName;
    public TextMeshProUGUI availability;
    public Transform itemGridParent;

    [Space(10)]
    public TextMeshProUGUI itemName;
    public TextMeshProUGUI itemDescription;
    public Image itemSprite;
    public Image itemTypeImage;
    public Button discardButton;
    public Button useEquipButton;

    [Space(10)]
    public PickUpInteractible usableGOPrefab;
    public PickUpInteractible matGOPrefab;
    public PickUpInteractible equipGOPrefab;

    protected CharacterData myPlayerData;
    protected int selectedFilter = 0;
    protected int currSelectedItem = 0;

    protected List<ItemSlotUI> itemButtons= new List<ItemSlotUI>();
    protected List<ItemWithBagPosition> currFilterItems= new List<ItemWithBagPosition>();
    protected Bag thisBag;

    // Start is called before the first frame update
    void Start()
    {
        myPlayerData = GameManager.instance.player.GetComponent<CharacterData>();
        ((Bag)myPlayerData.equipmentItems[(int)EquipType.backpack]).onItemChangedCallback += UpdateUI;
        itemButtons = itemGridParent.GetComponentsInChildren<ItemSlotUI>(true).ToList();
       
    }
    
    /// <summary>
    /// Method called when the iventory menu is open
    /// </summary>
    public override void UpdateThisMenu()
    {
        if (myPlayerData == null)
            Start();
        base.UpdateThisMenu();
        UpdateCurrFilterList(); // directly makes all the items on the currFilterItems
        thisBag = ((Bag)myPlayerData.equipmentItems[(int)EquipType.backpack]);
        
        if (thisBag)
        {
            // Change the name of the bag and availability total
            bagName.text = thisBag.myName;
            availability.text = "" + (thisBag.itemsInTheBag.Count) + "/" + thisBag.totalSpace;
            // Update the items elements
            UpdateNumberOfSlots();
            UpdateTheGeneralItemGrid();

            //Debug.Log("Item count in item buttons: " + itemButtons.Count);
            if (currFilterItems.Count==0)
            {
                itemName.text = "-";
                itemDescription.text = "-";
                itemSprite.enabled = false;
                discardButton.interactable = false;
                useEquipButton.interactable = false;
            }
        }
        else
        {
            // leave everything with -
            bagName.text = "-";
            availability.text = "0/0";
            
            for (int i = itemButtons.Count - 1; i > 0; i--)
            {
                itemButtons[i].gameObject.SetActive(false);
            }
            itemName.text = "-";
            itemDescription.text = "-";
            itemSprite.enabled = false ;
            discardButton.interactable = false;
            useEquipButton.interactable = false;
        }
    }

    /// <summary>
    /// Method called by the buttons to change the Filter type
    /// </summary>
    /// <param name="newFilter"></param>
    public void UpdateItemFilter(int newFilter)
    {
        // update the buttons?
        selectedFilter = newFilter;
        UpdateCurrFilterList();
    }

    /// <summary>
    /// Method called when the selected item is changed
    /// </summary>
    /// <param name="index"></param>
    public void ChangeSelectedItem(int index)
    {
        //Debug.Log("pre select in the iventory menu");
        currSelectedItem = index;
        UpdateTheGeneralItemGrid();
    }

    /// <summary>
    /// Method called to discard an Item from the inventory
    /// </summary>
    public void OnDiscardItem()
    {
        PickUpInteractible prefab;
        prefab = GameObject.Instantiate(currFilterItems[currSelectedItem].item.itemPrefab, myPlayerData.transform.position + myPlayerData.transform.forward,Quaternion.identity);

        // istantiate the item in the curr Selected Index
        prefab.itemInfo = currFilterItems[currSelectedItem].item;
        //the item will be removed from the inventory
        ((Bag)myPlayerData.equipmentItems[(int)EquipType.backpack]).RemoveItem(currFilterItems[currSelectedItem].item, currFilterItems[currSelectedItem].indexItem);
    }

    /// <summary>
    /// Method called to use or Equip the item from the inventory
    /// The item will be used or equipped, based on the type of class
    /// </summary>
    public void OnUseEquipItem()
    {
        //Debug.Log("Use-Equip item");
        // use or equip curr selected Item
        if (currFilterItems[currSelectedItem].item.type == ObjectType.usable)
        {
            ((UsableObject)currFilterItems[currSelectedItem].item).Use(GameManager.instance.player.GetComponent<CharController>());     
            //if it is monouse
            if (((UsableObject)currFilterItems[currSelectedItem].item).isMonouse)
            {
                ((Bag)myPlayerData.equipmentItems[(int)EquipType.backpack]).RemoveItem(currFilterItems[currSelectedItem].item, currFilterItems[currSelectedItem].indexItem);
            }
        }
        else // equippable
        {
            if (((Equipment)currFilterItems[currSelectedItem].item).equipType == EquipType.backpack) // it's a bag
            {
                Bag oldBag = ((Bag)myPlayerData.equipmentItems[(int)EquipType.backpack]);
                ((Bag)myPlayerData.equipmentItems[(int)EquipType.backpack]).RemoveEquipped();
                myPlayerData.equipmentItems[(int)EquipType.backpack] = (Bag)currFilterItems[currSelectedItem].item;
                ((Bag)myPlayerData.equipmentItems[(int)EquipType.backpack]).Add(oldBag);
            }
            else
            {
                EquipType requestedType = ((Equipment)currFilterItems[currSelectedItem].item).equipType;
                if (myPlayerData.equipmentItems[(int)requestedType]) // if there is one equipped already
                {
                    myPlayerData.equipmentItems[(int)requestedType].RemoveEquipped();
                }
                myPlayerData.equipmentItems[(int)requestedType] = (Equipment)currFilterItems[currSelectedItem].item;
                ((Bag)myPlayerData.equipmentItems[(int)EquipType.backpack]).RemoveItem(currFilterItems[currSelectedItem].item, currFilterItems[currSelectedItem].indexItem);
            }
            myPlayerData.GetComponent<CharController>().UpdateEquipmentModifiers(); // updates the modifiers after the new equipment had success
        }
    }


    /// <summary>
    /// Called when the inventory has an update
    /// </summary>
    protected void UpdateUI()
    {
        if (myPlayerData == null)
            Start();
        thisBag = (Bag)myPlayerData.equipmentItems[(int)EquipType.backpack];
        availability.text = "" + (thisBag.itemsInTheBag.Count) + "/" + thisBag.totalSpace;
        // update the list of items, because some items could be added, used or discarded
        UpdateCurrFilterList();
        // check if bagname Is changed > so if thebag changed
        UpdateNumberOfSlots();
     
        UpdateTheGeneralItemGrid();
        //Debug.Log("UPDATE UI: Update the inventory");
    }

    /// <summary>
    /// Method called to update the curr Filter
    /// </summary>
    /// <param name="newFilter">the new filter for the currFilter List. By default it's the all list</param>
    protected void UpdateCurrFilterList()
    {
        currFilterItems.Clear();
        thisBag = (Bag)myPlayerData.equipmentItems[(int)EquipType.backpack];
        if (thisBag)
        {
            switch (selectedFilter)
            {
                case 0: // all items
                    for(int i=0; i< thisBag.itemsInTheBag.Count; i++)
                    {
                        currFilterItems.Add( new ItemWithBagPosition(i, thisBag.itemsInTheBag[i]));
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// This method will select/ deselect the item, updating the sprites and the items of the item UISlots
    /// </summary>
    protected void UpdateTheGeneralItemGrid()
    {
        // select the first element if the selected is higher than the currect one
        if (currSelectedItem >= currFilterItems.Count)
            currSelectedItem = 0;

        // Add all the infos per each element
        for (int i = 0; i < currFilterItems.Count; i++)
        {
            // Select the currSelectedItem if present and lightly deselect the rest
            if (i != currSelectedItem)
            {
                //Debug.Log("[UPDATE THE GENERAL griD] index: " + i + ", buttons count: " + itemButtons.Count + ", currFilter count: "+ currFilterItems.Count);
                itemButtons[i].mainButtonImage.color = unSelectedItem;
            }
            else
            {
                itemButtons[i].mainButtonImage.color = selectedItem;
                itemTypeImage.sprite = typeIconSprites.GetTypeIcon( currFilterItems[i].item.type);
                itemName.text = currFilterItems[i].item.myName;
                itemDescription.text = currFilterItems[i].item.desc;
                itemSprite.enabled = true;
                itemSprite.sprite = currFilterItems[i].item.icon;
                if (currFilterItems[i].item.type != ObjectType.key)
                    discardButton.interactable = true;
                else
                    discardButton.interactable = false;
                if (currFilterItems[i].item.type == ObjectType.equipment || currFilterItems[i].item.type == ObjectType.usable)
                    useEquipButton.interactable = true;
                else
                    useEquipButton.interactable = false;
            }
            itemButtons[i].Inizialize(this,i, currFilterItems[i].item); // so we know what it is used
        }
    }

    /// <summary>
    /// Updated the slots in case it is different from the number of slots required
    /// </summary>
    protected void UpdateNumberOfSlots()
    {
        if (itemButtons.Count != currFilterItems.Count)
        {
            if (itemButtons.Count < currFilterItems.Count) // need to add a bit of items prefabs
            {
                for (int i = itemButtons.Count; i < currFilterItems.Count; i++)
                {
                    itemButtons.Add(Instantiate(itemSlotPrefab, itemGridParent));
                }
            }
            else
            {
                for (int i = itemButtons.Count; i >= currFilterItems.Count; i--)
                {
                    itemButtons[i-1].gameObject.SetActive(false);
                }
            }
        }
    }


}

/// <summary>
/// Struct containing the item and the index of the item in the original list
/// </summary>
[System.Serializable]
public struct ItemWithBagPosition
{
    /// <summary>
    /// The index in the all items in bag
    /// </summary>
    public int indexItem;
    public SimpleObject item; 

    public ItemWithBagPosition(int _index, SimpleObject _item)
    {
        indexItem = _index;
        item = _item;
    }
}
