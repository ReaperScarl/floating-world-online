﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class called from the CharacterEquipment to get the stats read
/// </summary>
public class StatWriterInMenuHelper
{
    /// <summary>
    /// Array of strings that will be used with the names of the stats.
    /// They are the same of the character data stats
    /// </summary>
    public string[] statNames = new string[]
    {
        "Max HP",
        "Armor",
        "Physical damage",
    };

    protected CharacterData playerData;

    public StatWriterInMenuHelper(CharacterData _playerData)
    {
        playerData = _playerData;
    }

    /// <summary>
    /// Method called to get the content of the column
    /// </summary>
    /// <param name="column">0= name of the attribute, 1=base values, 2= bonus value</param>
    /// <returns></returns>
    public string GetColumn(int column)
    {
        string stringToSend="";
        switch (column)
        {
            // name of the attri
            case 0:
                stringToSend += $"<b>{statNames[0]}</b> :   \n";
                stringToSend += $"<b>{statNames[1]}</b> :   \n";
                stringToSend += $"<b>{statNames[2]}</b> :   \n";
                break;
               
            case 1:
                stringToSend += $"<b>{playerData.CurrMaxHP}</b>    \n";
                stringToSend += $"<b>{playerData.CurrArmor}</b>    \n";
                stringToSend += $"<b>{playerData.CurrPhysDmg}</b>   \n";
                break;
            // base value of the attri
            case 2:
                stringToSend += $"{playerData.baseMaxHP}    \n";
                stringToSend += $"{playerData.baseArmor}    \n";
                stringToSend += $"{playerData.basePhysicalDamage}   \n";
                break;
            // bonus value ofthe attri
            case 3:
                stringToSend += $"<color=#0000ff>+ {playerData.bonusMaxHP}</color>  \n";
                stringToSend += $"<color=#0000ff>+ {playerData.bonusArmor}</color>  \n";
                stringToSend += $"<color=#0000ff>+ {playerData.basePhysicalDamage}</color>  \n";
                break;
        }

        return stringToSend;
    }

}
