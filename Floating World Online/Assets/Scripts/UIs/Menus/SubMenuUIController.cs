﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Generic Class responsible for a specific sub menu of the in game Menu.
/// With this class, an initial update of the current UI will be made when the menu is selected
/// </summary>
public class SubMenuUIController : MonoBehaviour
{
    /// <summary>
    /// Method called when the sub menu is chosen to update the menu when needed
    /// </summary>
    public virtual void UpdateThisMenu()
    {

    }
}
