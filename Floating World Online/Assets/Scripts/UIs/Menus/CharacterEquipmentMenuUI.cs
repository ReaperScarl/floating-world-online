﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Class  responsible for the UI character equipment menu
/// </summary>
public class CharacterEquipmentMenuUI : SubMenuUIController
{
    // TO DO: change them in an array, with the same index between equipments, enum and buttons

    public ItemEquipSlotUI[] equipmentButtons; // the order is the same as the equip type enumerator

    public Sprite placeholderSprite;

    public TextMeshProUGUI charAttributesname;
    public TextMeshProUGUI charAttributesTotValue;
    public TextMeshProUGUI charAttributesBaseValue;
    public TextMeshProUGUI charAttributesBonusValue;


    protected CharacterData myPlayerData;
    protected StatWriterInMenuHelper statWriterHelper;
    

    private void Start()
    {
        if (myPlayerData == null)
        {
            myPlayerData = GameManager.instance.player.GetComponent<CharacterData>();
            statWriterHelper = new StatWriterInMenuHelper(myPlayerData);
        }
    }


    /// <summary>
    /// This will update the characterEquipMenu
    /// </summary>
    public override void UpdateThisMenu()
    {
        base.UpdateThisMenu();
        if (myPlayerData == null)
            Start();
        for (int i=0; i<equipmentButtons.Length; i++)
        {
            equipmentButtons[i].SetMenu(this);
            if (myPlayerData.equipmentItems[i])
            {
                equipmentButtons[i].ActivateThisSlot(myPlayerData.equipmentItems[i].icon);
            }
            else
            {
                equipmentButtons[i].DeactivateThisSlot();
            }
        }
        charAttributesname.text = statWriterHelper.GetColumn(0);
        charAttributesTotValue.text = statWriterHelper.GetColumn(1);
        charAttributesBaseValue.text = statWriterHelper.GetColumn(2);
        charAttributesBonusValue.text = statWriterHelper.GetColumn(3);
    }

    /// <summary>
    /// Method called to Unequip 
    /// </summary>
    /// <param name="elementToRemove"></param>
    public void Unequip(EquipType elementToRemove)
    {
        CharacterData data = GameManager.instance.player.GetComponent<CharacterData>();
        if (elementToRemove == EquipType.backpack)
        {
            equipmentButtons[(int)EquipType.backpack].DeactivateThisSlot();
            data.equipmentItems[(int)EquipType.backpack].RemoveEquipped(true);
        }
        else
        {
            if (data.equipmentItems[(int)EquipType.backpack] && 
                ((Bag) data.equipmentItems[(int)EquipType.backpack]).spaceAvailable >= data.equipmentItems[(int)elementToRemove].occupiedSpace)
                data.equipmentItems[(int)elementToRemove].RemoveEquipped();
            else
                data.equipmentItems[(int)elementToRemove].RemoveEquipped(true);
            equipmentButtons[(int)elementToRemove].DeactivateThisSlot();
        }
        data.equipmentItems[(int)elementToRemove] = null;
        data.GetComponent<CharController>().UpdateEquipmentModifiers();
    }



}
