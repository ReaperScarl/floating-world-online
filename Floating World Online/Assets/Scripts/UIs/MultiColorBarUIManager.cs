﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiColorBarUIManager : BarUIManager
{
    public Color[] colorStatuses = new Color[]
    {
        Color.green,
        Color.yellow,
        Color.red
    };


    public float firstThreshold;
    public float secThreshold;

    /// <summary>
    /// Here, the update bar will change color with 3 thresholds.
    /// </summary>
    /// <param name="currValue"></param>
    /// <param name="maxValue"></param>
    public override void UpdateBar(float currValue, float maxValue)
    {
        base.UpdateBar(currValue, maxValue);
        if (handler.fillAmount < firstThreshold)
        {
            if (handler.color != colorStatuses[0])
                handler.color = colorStatuses[0];
        }
        else
        {
            if(handler.fillAmount>=firstThreshold && handler.fillAmount < secThreshold)
            {
                if (handler.color != colorStatuses[1])
                    handler.color = colorStatuses[1];
            }
            else
            {
                if (handler.color != colorStatuses[2])
                    handler.color = colorStatuses[2];
            }
        }
    }

}
