﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Class Responsible for the feedback messages.
/// </summary>
public class FeedbackListManager : MonoBehaviour
{
    public FeedbackElementUI feedbackElementPrefab;

    public List<FeedbackElementUI> messagesActive;

    public List<FeedbackElementUI> messagesDeactive;


    public void Start()
    {
        messagesActive = new List<FeedbackElementUI>();
        messagesDeactive= GetComponentsInChildren<FeedbackElementUI>(true).ToList();
    }



    public void SendMessage(string message, FeedbackType type)
    {
        // check if there is a message set to false
        if (messagesDeactive.Count > 0)
        {

            messagesDeactive[0].WriteFeedback(message, type,this);
            messagesActive.Add(messagesDeactive[0]);
            messagesDeactive.RemoveAt(0);

        }
        else // add more prefabs
        {

        }
    }
     

}
