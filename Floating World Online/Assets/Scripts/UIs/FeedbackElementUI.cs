﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Class that will be used to make a feedback UI element for when it is needed to write something
/// on screen.
/// </summary>
public class FeedbackElementUI : MonoBehaviour
{
    public Image backgroundImage;

    public TextMeshProUGUI feedbackText;

    public float timeToShow = 0.5f;
    public float timeOfLife = 3;

    public Color[] feedbakBGColors = new Color[]{
        Color.white,    // normal color
        Color.yellow,   // warning color
        Color.red       // danger color
    };

    protected WaitForSeconds waitFeedBackToDie;
    protected FeedbackListManager man;

    public void WriteFeedback(string message, FeedbackType type, FeedbackListManager manager)
    {
        gameObject.SetActive(true);
        waitFeedBackToDie = new WaitForSeconds(timeOfLife);
        man = manager;
        StartCoroutine(ShowFeedBack(message, type));

    }


    IEnumerator ShowFeedBack(string message, FeedbackType type)
    {
        float currTime = 0;
        backgroundImage.color = feedbakBGColors[(int)type];
        backgroundImage.color = new Color( backgroundImage.color.r, backgroundImage.color.g, backgroundImage.color.b,0) ;
        backgroundImage.enabled = true;

        while (currTime < timeToShow)
        {
            yield return null;
            currTime += Time.deltaTime;
            backgroundImage.color = new Color(backgroundImage.color.r, backgroundImage.color.g, backgroundImage.color.b, currTime/timeToShow);
        }
        feedbackText.enabled = true;
        feedbackText.text = message;
        yield return waitFeedBackToDie;
        man.messagesActive.Remove(this);
        man.messagesDeactive.Add(this);
        backgroundImage.enabled = false;
        feedbackText.enabled = false;

        gameObject.SetActive(false);
    }

}

/// <summary>
/// Feedback type, for when it is needd to write with the precise color of background
/// </summary>
public enum FeedbackType
{
    normal,
    warning,
    danger
}
