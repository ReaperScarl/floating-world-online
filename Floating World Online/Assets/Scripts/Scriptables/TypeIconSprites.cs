﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that will have the icons for the type of object
/// In this way, you don't need to set for every object the sprites inside the game
/// </summary>
[CreateAssetMenu(fileName = "IconSprites", menuName = "ForSprites/TypeIconSprites", order = 1)]
public class TypeIconSprites : ScriptableObject
{
    public Sprite[] itemTypeIcons;

    /// <summary>
    /// Method called to return the type icon needed based on the type icon enum sent.
    /// </summary>
    /// <param name="myType"> the icon type requested</param>
    /// <returns></returns>
    public Sprite GetTypeIcon(ObjectType myType)
    {
        return itemTypeIcons[(int)myType];
    }

}
