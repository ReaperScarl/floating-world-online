﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class containing the datas for the player
/// </summary>
public class CharacterData : MonoBehaviour
{
    #region character personalization attributes
    // here all the stuff about the character personalization (blend shapes etc)

    #endregion

    #region essential attributes
    public float currHP; // the current hp of the player

    // BASE STATS. they will stay the same or change based on the level, but they other than that, they are stable

    public float baseMaxHP;
    public float baseArmor;
    public float basePhysicalDamage;

    // bONUS STATS (given by the equipment modifiers) 
    //-> when new bonus mod are given, you need to add an handler and put it in the general StatModManager
    public float bonusMaxHP;
    public float bonusArmor;
    public float bonusPhysDmg;


    // PROPERTIES
    public float CurrMaxHP { get { return baseMaxHP + bonusMaxHP; } }

    /// <summary>
    ///  Property: the value of the curr armor, given by the base stats and the bonus given by the equip
    /// </summary>
    public float CurrArmor 
    {
        get {  return baseArmor + bonusArmor; } 
    }

    public float CurrPhysDmg { get { return basePhysicalDamage + bonusPhysDmg; } }

    #endregion

    #region survivalAttributes

    public float currHunger;
    public float maxHunger;
    public float hungerPerMinute; // hunger value that goes up every second

    public float currThirst;
    public float maxThirst;
    public float thirstPerMinute; // thirst value that goes up every second

    public float currSleep;
    public float maxSleep;
    public float sleepPerMinute; // sleep value that goes up every second
    #endregion

    public Equipment [] equipmentItems; // this is on the same order of the equipType enum
}

[Flags] // a way to use the enums for bit fields
public enum PassiveStatType
{
    none=0,
    armor=1,
    physDmg=2,
    maxHP=4,
}

