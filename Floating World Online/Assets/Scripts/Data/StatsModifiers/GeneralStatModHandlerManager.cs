﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the manager of all the handlers. This will be called to make the modifier etc
/// </summary>
public class GeneralStatModHandlerManager
{
    protected _StatsModifierHandler [] handlers;

    public GeneralStatModHandlerManager()
    {
          // get all the handlers -> here will be added the new stats modifiers when they are ready (eventually)
          handlers= new _StatsModifierHandler [] {
                new MaxHPModifierHandler(),
                new ArmorModifierHandler(),
                new PhysDmgModifierHandler()
            };

        // make all the connection between them so the next to i is i+1 etc
        for(int i=0; i<handlers.Length-1; i++)
        {
            handlers[i].SetNextStatModHandler(handlers[i + 1]);
        }
    }

    /// <summary>
    /// Method called after the construction, to update the data stat bonus modifiers
    /// </summary>
    /// <param name="equip"></param>
    /// <param name="data"></param>
    public void UpdateModifiers(Equipment equip, CharacterData data)
    {
        handlers[0].UpdateModifier(data, equip);
    }

}
