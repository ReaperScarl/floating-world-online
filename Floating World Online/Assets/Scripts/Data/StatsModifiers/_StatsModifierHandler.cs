﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstract class that will handle the modification
/// </summary>
public abstract class _StatsModifierHandler
{
    protected _StatsModifierHandler _nextStatModHandler;
    protected PassiveStatType _statsTohandle;
    
    public _StatsModifierHandler(PassiveStatType statToHandle)
    {
        _statsTohandle = statToHandle;
    }

    public void SetNextStatModHandler(_StatsModifierHandler handler)
    {
        _nextStatModHandler = handler;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="equipItem"></param>
    public void UpdateModifier(CharacterData data, Equipment equipItem)
    {
        Debug.Log($"Equip name: {equipItem.myName}. BonusStat: {equipItem.bonusStats.Count} ");
        foreach(EquipStatsEffect effect in equipItem.bonusStats)
        {
            if (_statsTohandle == (effect.stat & _statsTohandle) )
            {
                Debug.Log($"{this.GetType().Name} providing {this._statsTohandle} services");
                // update the modifier
                AddModifierValue(effect, data);
            }
        }

        if (_nextStatModHandler == null)
            return;
        else
            _nextStatModHandler.UpdateModifier(data, equipItem);
    }

    /// <summary>
    /// Method called that will change depending on every handle
    /// </summary>
    /// <param name="effect"></param>
    /// <param name="data"></param>
    protected virtual void AddModifierValue (EquipStatsEffect effect, CharacterData data)
    {

    }

}
