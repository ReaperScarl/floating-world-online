﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class in a chain of responsability, that manage the bonus value for the armor
/// </summary>
public class ArmorModifierHandler : _StatsModifierHandler
{
    public ArmorModifierHandler() : base(PassiveStatType.armor){}

    protected override void AddModifierValue(EquipStatsEffect effect, CharacterData data)
    {
        base.AddModifierValue(effect, data);
        data.bonusArmor += effect.value;
    }
}
