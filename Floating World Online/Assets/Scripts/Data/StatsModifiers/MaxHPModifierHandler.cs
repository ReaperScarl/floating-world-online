﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This handler will manage the MaxHP modifier effects
/// </summary>
public class MaxHPModifierHandler: _StatsModifierHandler
{
    public MaxHPModifierHandler() : base(PassiveStatType.maxHP) { } // constructor, passing the statType directly

    protected override void AddModifierValue(EquipStatsEffect effect, CharacterData data)
    {
        base.AddModifierValue(effect, data);
        data.bonusMaxHP += effect.value;
    }

}
