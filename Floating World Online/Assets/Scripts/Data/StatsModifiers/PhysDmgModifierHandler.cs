﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class inside of a chain of responsability, to manage the changes in the bonus physical Dmg 
/// </summary>
public class PhysDmgModifierHandler : _StatsModifierHandler
{
    public PhysDmgModifierHandler() : base(PassiveStatType.physDmg) { }

    protected override void AddModifierValue(EquipStatsEffect effect, CharacterData data)
    {
        base.AddModifierValue(effect, data);
        data.bonusPhysDmg += effect.value;
    }

}
