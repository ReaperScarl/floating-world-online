﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

/// <summary>
/// Class that child of SimpleObject 
/// </summary>
public abstract class Equipment : SimpleObject
{
    public EquipType equipType;
    public List<EquipStatsEffect> bonusStats; // the list is in the Usable object script. The stat type is inside the CharData

    public abstract void Equip(CharController playerController);

    /// <summary>
    /// TO DO
    /// If there is bag and there is space, go there, otherwise put it in on the ground
    /// </summary>
    public virtual void RemoveEquipped(bool directly=false)
    {
        CharacterData data = GameManager.instance.player.GetComponent<CharacterData>();
        if (directly)
        {
            // discard directly the element
            PickUpInteractible prefab;

            prefab = GameObject.Instantiate(itemPrefab, data.transform.position + data.transform.forward, Quaternion.identity);

            // istantiate the item in the curr Selected Index
            prefab.itemInfo = this;
        }
        else
        {
            // put the equipment in the bag
            Bag playerBag = (Bag)data.equipmentItems[(int)EquipType.backpack];
            if (playerBag && playerBag.GetAvailableSpace() >= occupiedSpace)
            {
                playerBag.Add(this);
            }
        }
    }
}

// General Type of your equipment
public enum EquipType
{
    shoulders,
    helmet,
    arms,
    gauntlets,
    legs,
    boots,
    backpack,
    leftWeaponA,
    leftWeaponB,
    rightHandA,
    rightHandB,
}
/// <summary>
/// struct responsible for the stat type and the value change for that stat
/// </summary>
[System.Serializable]
public struct EquipStatsEffect
{
    public PassiveStatType stat;
    public float value;
}