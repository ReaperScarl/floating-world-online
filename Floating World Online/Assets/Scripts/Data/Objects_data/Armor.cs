﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
[CreateAssetMenu(fileName = "New Item", menuName = "Data/Iventory/ArmorItem")]
public class Armor : Equipment
{
    protected CharacterData charData;

    public override void Equip(CharController playerController)
    {
        charData = playerController.myData;

        if (charData.equipmentItems[(int)equipType]) // if there is one equipped already
        {
            charData.equipmentItems[(int)equipType].RemoveEquipped();
        }
        charData.equipmentItems[(int)equipType] = (Equipment)this;
        playerController.UpdateEquipmentModifiers();

    }

    public override void RemoveEquipped(bool directly = false)
    {
        base.RemoveEquipped(directly);
    }
}


