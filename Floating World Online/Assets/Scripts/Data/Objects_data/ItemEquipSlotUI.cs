﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class ItemEquipSlotUI : ItemSlotUI
{
    public Button removeButton;

    public EquipType myTypeButton;


    public void SetMenu(CharacterEquipmentMenuUI menu)
    {
        if(subMenu==null)
            subMenu = menu;
    }

    /// <summary>
    /// Method called from the X remove buttonin the Slot
    /// </summary>
    public void OnRemoveEquippedItem()
    {
        ((CharacterEquipmentMenuUI)subMenu).Unequip(myTypeButton);
    }

    public void ActivateThisSlot(Sprite itemIcon)
    {
        removeButton.gameObject.SetActive(true);
        itemIconImage.sprite = itemIcon;
        itemIconImage.enabled = true;
        
    }

    public void DeactivateThisSlot()
    {
        removeButton.gameObject.SetActive(false);
        itemIconImage.enabled = false;
    }

}
