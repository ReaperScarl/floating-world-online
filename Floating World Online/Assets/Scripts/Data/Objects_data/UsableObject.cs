﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Usable object that can be used by the player
/// </summary>
[CreateAssetMenu(fileName = "New Item", menuName = "Data/Iventory/UsableItem")]
public class UsableObject : SimpleObject
{
    public bool isMonouse = true;

    /// <summary>
    /// Effects that the usable object has
    /// </summary>
    public Effect [] effects;
    public ActiveEffect[] activeEffects; // effects given


    /// <summary> the method called when the object is used </summary>
    /// <param name="myPlayer"></param>
    public void Use(CharController myPlayer)
    {
        // uses the effects to the player
        myPlayer.SetEffects(effects);
        foreach(ActiveEffect effect in activeEffects)
        {
            // create a clone of the effect without using the same reference, in theory
            myPlayer.activeEffects.Add(Instantiate(effect)); 
        }
    }
}

/// <summary>
/// struct responsible for the stat type and the value change for that stat
/// </summary>
[Serializable]
public struct Effect 
{
    public ActiveStatType stat;
    public float value;
}

public enum ActiveStatType
{
    hp=0,
    hunger = 1,
    thirst = 2,
    sleep = 4,
}


