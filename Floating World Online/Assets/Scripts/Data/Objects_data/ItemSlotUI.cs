﻿using Boo.Lang.Environments;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class responsible for the infos on the button of the item in the inventory UI
/// </summary>
public class ItemSlotUI : MonoBehaviour
{
    public Image mainButtonImage;
    public Image itemIconImage;
    public int indexIn;
    protected SimpleObject itemInfo;

    protected SubMenuUIController subMenu;


    public void Inizialize(InventoryUIManager iventory,int newIndex, SimpleObject _itemInfo)
    {
        if(subMenu == null)
            subMenu = iventory;
        indexIn = newIndex;
        itemIconImage.enabled = true;
        itemInfo = _itemInfo;
        itemIconImage.sprite = itemInfo.icon;
        gameObject.SetActive(true);
    }

    public void Select()
    {
        ((InventoryUIManager)subMenu).ChangeSelectedItem(indexIn);

    }

}
