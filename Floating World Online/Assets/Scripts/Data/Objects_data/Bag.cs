﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

/// <summary>
/// Class responsible for the bag, that will have the items inside
/// </summary>
[CreateAssetMenu(fileName = "new Bag", menuName = "Data/Iventory/bag")]
public class Bag : Equipment
{
    public bool isBagInside = false;
    public int totalSpace;
    //public int rowWidth = 6;

    public List<SimpleObject> itemsInTheBag = new List<SimpleObject>();

    public int spaceAvailable;
    //public bool[,] occupationMatrix; //DEPRECATED
    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    private void Awake()
    {
        //occupationMatrix = new bool[(int)Mathf.CeilToInt(totalSpace / rowWidth), rowWidth];
        if(itemsInTheBag.Count==0)
            spaceAvailable = totalSpace;
        else
        {
            spaceAvailable = totalSpace;
            foreach(SimpleObject item in itemsInTheBag)
            {
                spaceAvailable -= item.occupiedSpace;
            }
        }
    }

    public override void Equip(CharController playerController)
    {
        if (playerController.myData.equipmentItems[(int)EquipType.backpack])
            playerController.myData.equipmentItems[(int)EquipType.backpack].RemoveEquipped(); // should put it in the bag
        playerController.myData.equipmentItems[(int)EquipType.backpack] = this;
    }

    /// <summary>
    /// This method is Called to discard or remove 
    /// </summary>
    /// <param name="directly"></param>
    public override void RemoveEquipped(bool directly = false)
    {
        base.RemoveEquipped(directly);
    }

    /// <summary>
    /// Method called to add an item in the bag
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public bool Add(SimpleObject item)
    {
        if (spaceAvailable >= item.occupiedSpace)
        {
            Debug.Log("space av: " + spaceAvailable);
            if (item.IsPickupable())
            {
                // Create a struct object with the item and the position, add in the inventory of this bag, return true
                itemsInTheBag.Add(item);
                spaceAvailable -= item.occupiedSpace;
                if (item.GetType() == typeof(Bag))
                    isBagInside = true;
                if (onItemChangedCallback != null)
                    onItemChangedCallback.Invoke();
                return true;
            }
            else
            {
                GameManager.instance.player.GetComponent<CharController>().SendFeedbackMessage("the item is not pickable!",FeedbackType.warning);
            }
        }
        GameManager.instance.player.GetComponent<CharController>().SendFeedbackMessage("The Bag is full!", FeedbackType.warning);
        return false;
    }

    /// <summary>
    /// Item Called to discard an object. The object will be given in output, so that it can be istantiated elsewhere
    /// </summary>
    /// <param name="itemToDiscard"></param>
    /// <returns></returns>
    public SimpleObject RemoveItem(SimpleObject itemToDiscard, int index=-1)
    {
        if (index == -1)
        {
            //RemoveElementInOccupationMatrix(itemToDiscard.topLeftPos,item.occupiedSpace);
            itemsInTheBag.Remove(itemToDiscard);
        }
        else
        {
            itemsInTheBag.RemoveAt(index);
        }
        
        //CheckForBagsInside(); //DEPRECATED
        if(onItemChangedCallback!=null)
            onItemChangedCallback.Invoke();
        spaceAvailable += itemToDiscard.occupiedSpace;
        return itemToDiscard;
    }

    public override bool IsPickupable()
    {
        return !isBagInside;
    }

    public int GetAvailableSpace()
    {
        spaceAvailable = totalSpace;
        foreach(SimpleObject item in itemsInTheBag)
        {
            spaceAvailable -= item.occupiedSpace;
        }
        return spaceAvailable;
    }


    /// <summary>
    /// DEPRECATED: occupied space is now just a number-float, instead of actually a position in the bag
    /// This method will get the first free position found in the matrix for a object with the occupied space
    /// </summary>
    /// <param name="occSpace"></param>
    /// <returns></returns>
    /*
    public Vector2 GetFreePosition(Vector2 occSpace)
    {
        bool isSpotFound;
        for (int i = 0; i < occupationMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < occupationMatrix.GetLength(1); j++)
            {
                isSpotFound = true;
                //is this available? If yes, check the ones nearby based on the occup space
                if (occupationMatrix[i, j])
                {
                    if (occupiedSpace.x == 1 && occupiedSpace.y == 1)
                        return new Vector2(i, j);

                    //it will check if the occupied space is 1 for the x (and we know this is available already)
                    //In case it is bigger, we also need to check if there is enough space at least
                    if ((i + occupiedSpace.x < occupationMatrix.GetLength(0))) 
                    {
                        // check if the line is available
                        for(int k=i+1; k<=i+occupiedSpace.x-1; k++)
                        {
                            if (!occupationMatrix[k, j])
                            {
                                isSpotFound = false;
                                break;
                            }      
                        }
                        // this 
                        if (isSpotFound && ( j + occupiedSpace.y< occupationMatrix.GetLength(1)) )
                        {
                            // check if the line is available
                            for (int k = j+1; k <= j + occupiedSpace.y - 1; k++)
                            {
                                if (!occupationMatrix[i, k])
                                {
                                    isSpotFound = false;
                                    break;
                                }
                                else
                                {
                                    if (k == j + occupiedSpace.y)
                                        return new Vector2(i, j);
                                }
                            }
                        }
                    }    
                }
            }
        }
        return -Vector2.one; // wrong value if the position is not found
    }
    */

    /// <summary>
    /// DEPRECATED: no more occupation matrix
    /// Method called to occupy the space in the occupation space
    /// </summary>
    /// <param name="pos">top left of the object</param>
    /// <param name="space">the occupied space of the object</param>
    /*
    protected void AddElementInOccupationMatrix(Vector2 pos, Vector2 space)
    {
        for(int i=(int)pos.x; i<pos.x+space.x; i++)
        {
            for (int j = (int)pos.y; j < pos.y + space.y; j++)
            {
                occupationMatrix[i, j] = false;
            }
        }
        spaceAvailable -= (int)(space.x * space.y);
    }
    */

    /// <summary>
    /// DEPRECATED: the matrix is no longer used
    /// Frees the bool in the positions occupied and no longer in use.
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="space"></param>
    /*
    protected void RemoveElementInOccupationMatrix(Vector2 pos, Vector2 space)
    {
        for (int i = (int)pos.x; i < pos.x + space.x; i++)
        {
            for (int j = (int)pos.y; j < pos.y + space.y; j++)
            {
                occupationMatrix[i, j] = true;
            }
        }
        spaceAvailable += (int)(space.x * space.y);
    }
    */

    /// <summary>
    /// DEPRECATED: now the objects now don't have a position anymore
    /// Method called to check if there are bags inside this bag.
    /// At the first bag, it will return, not changing the value.
    /// If no bags are found, the isbagInside will be put to false
    /// </summary>
    /*
    protected void CheckForBagsInside()
    {
        foreach(ItemPosInBag item in itemsInTheBag)
        {
            if (item.item.GetType() == typeof(Bag))
                return;
        }
        isBagInside = false;
    }
    */

    /// <summary>
    /// DEPRECATED: no longer using the occuation matrix
    /// Checks if the space at pos is free. If it is, it will return true, otherwise false
    /// </summary>
    /// <param name="pos">the top left position</param>
    /// <param name="space">the space that will be occupied</param>
    /// <returns></returns>
    /*
    protected bool CheckForAvailableSpace(Vector2 pos,Vector2 space)
    {
        for (int i = (int)pos.x; i < pos.x + space.x; i++)
        {
            for (int j = (int)pos.y; j < pos.y + space.y; j++)
            {
                if (!occupationMatrix[i, j])
                    return false;
            }
        }
        return true;
    }
    */

    [System.Serializable]
    //The item with the top left position of the item in the bag
    public struct ItemPosInBag
    {
        public Vector2 topLeftPos;
        public SimpleObject item;
        public ItemPosInBag(Vector2 _pos, SimpleObject _item)
        {
            topLeftPos = _pos;
            item = _item;
        }
    }
}
