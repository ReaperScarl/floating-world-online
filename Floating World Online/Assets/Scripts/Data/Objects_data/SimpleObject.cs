﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple General class with all the infos about a simple object, 
/// like the id, desc etc
/// </summary>
[CreateAssetMenu(fileName ="New Item",menuName ="Data/Iventory/Item")]
public class SimpleObject : ScriptableObject
{
    public string ID;
    public string myName;
    public string desc;

    //public string creator;
    public Sprite icon;
    public int occupiedSpace;
    public ObjectType type;
    public Rarity myRarity;
    public PickUpInteractible itemPrefab;

    /// <summary>
    /// Method called to say if that object is pickup able or not. Usually it is, but the bag might not be
    /// </summary>
    /// <returns></returns>
    public virtual bool IsPickupable()
    {
        return true;
    }
}

/// <summary> The type of the generic object </summary>
public enum ObjectType
{
    equipment= 0,
    raw=1,
    usable=2,
    key=3,
}

/// <summary> The rarity of the generic object </summary>
public enum Rarity
{
    common=0,
    uncommon,
    rare,
    veryRare,
    legenday
}